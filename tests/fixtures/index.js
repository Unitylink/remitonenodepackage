'use strict'

const xmlResponses = () => ({
  getRates: `<?xml version="1.0" encoding="utf-8"?><response><status>SUCCESS</status>
    <result><rates><rate><destination_country>Canada</destination_country><source_currency>EUR</source_currency>
    <destination_currency>CAD</destination_currency><account>1.444907</account><cash_collection>1.444851</cash_collection>
    <card>1.444767</card><home_delivery>1.444921</home_delivery><utility_bill>1.445002</utility_bill>
    <mobile_transfer>1.444899</mobile_transfer><wallet_transfer>1.444950</wallet_transfer></rate></rates></result></response>`
})

module.exports = xmlResponses

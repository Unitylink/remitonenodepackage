'use strict'

const nock = require('nock')

const baseUrl = 'https://unitylinknew.remitone.com/ws/'

const mockRequests = ({ pin, username, password }) => ({ group, method, params = {}, results = '' }) => {
  let parameters = ''
  for (const key in params) {
    parameters = (parameters !== '') ? `${key}=${params[key]}&${parameters}` : `${key}=${params[key]}`
  }

  parameters = parameters !== '' ? `${parameters}&pin=${pin}&username=${username}&password=${password}` : `pin=${pin}&username=${username}&password=${password}`

  return nock(baseUrl, {
    reqheaders: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }).persist()
    .post(`/${group}/${method}`, parameters.trim())
    .reply(201, results)
}

module.exports = mockRequests

/* eslint-disable no-undef */
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const xml2js = require('xml2js')
const ulRemitOne = require('../index')
const mockRequests = require('./mocks')
const xmlResponses = require('./fixtures/index')

chai.use(chaiAsPromised)
const { expect } = chai
const parser = new xml2js.Parser({ ignoreAttrs: true, explicitArray: false })

const baseUrl = 'https://unitylinknew.remitone.com/ws'
const password = '123456'
const pin = '1234'
const username = 'username'
const group = 'rates'
const method = 'getRates'

describe('Make request to remitone', () => {
  describe('Success', () => {
    const mockInstance = mockRequests({ pin, username, password })

    const params = { dest_country: 'ghana' }

    const results = xmlResponses().getRates

    const remitOneInstance = ulRemitOne({ baseUrl, password, pin, username })

    let scope

    describe('Request with parameters', () => {
      beforeEach(() => {
        scope = mockInstance({ group, method, params, results })
      })

      it('should successfully make a call to remitOne', async () => {
        await remitOneInstance({ group, method, params })
        return expect(scope.isDone()).equal(true)
      })

      it('should return results in a json format', async () => {
        const jsonResult = await parser.parseStringPromise(results)
        return expect(
          remitOneInstance({ group, method, params })
        ).eventually.eql(jsonResult)
      })
    })

    describe('Request without parameters', () => {
      before(() => {
        scope = mockInstance({ group, method, results })
      })
      it('should not fail when parameters are not provided', async () => {
        await remitOneInstance({ group, method })
        return expect(scope.isDone()).equal(true)
      })
    })
  })

  describe('Failure', () => {
    let remitOneInstance

    before(() => {
      remitOneInstance = ulRemitOne({ baseUrl, pin, username, password })
    })

    it('should fail if baseUrl is not provided', () => {
      return expect(() => ulRemitOne({ password, pin, username })).throws(
        'baseUrl is required'
      )
    })

    it('should fail if password is not provided', () => {
      return expect(() => ulRemitOne({ baseUrl, pin, username })).throws(
        'password is required'
      )
    })

    it('should fail if pin is not provided', () => {
      return expect(() => ulRemitOne({ baseUrl, password, username })).throws(
        'pin is required'
      )
    })

    it('should fail if username is not provided', () => {
      return expect(() => ulRemitOne({ baseUrl, password, pin })).throws(
        'username is required'
      )
    })

    it('should fail if group is not provided', () => {
      return expect(() => remitOneInstance({ method })).throws(
        'group is required'
      )
    })

    it('should fail if method is not provided', () => {
      return expect(() => remitOneInstance({ group })).throws(
        'method is required'
      )
    })
  })
})

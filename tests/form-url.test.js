const { expect } = require('chai')
const formUrl = require('../form-url')

// eslint-disable-next-line no-undef
describe('Form url', () => {
  const baseUrl = 'https://unitylinknew.remitone.com/ws'
  const group = 'rates'
  const method = 'getRates'
  // eslint-disable-next-line no-undef
  it('should be a function', () => {
    return expect(() => formUrl).to.be.a('function')
  })

  // eslint-disable-next-line no-undef
  it('should form the right request url', () => {
    return expect(formUrl(baseUrl, group, method)).to.be.equal(
      'https://unitylinknew.remitone.com/ws/rates/getRates'
    )
  })
})

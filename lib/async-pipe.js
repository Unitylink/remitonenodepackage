'use strict'

const asyncPipe =
  (...fns) =>
  (param) =>
    fns.reduce(async (result, next) => next(await result), param)

module.exports = asyncPipe

'use strict'

const got = require('got')
const maxRetryAfterInSeconds = 5000
const retryLimit = 3

const makeCallToRemitOne = ({ uri, body, query }) => {
  return got.post(uri, {
    form: body,
    query,
    retry: {
      limit: retryLimit,
      maxRetryAfter: maxRetryAfterInSeconds
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

module.exports = makeCallToRemitOne

'use strict'

const defaultErrorFunc = (str) => {
  throw new Error(`${str} is required`)
}

/**
 *A function to ensure params are passed
 *
 * @param {Function}  errorFunc
 * @param {String} str
 * @returns str | errorFunc
 */

function required(errorFunc = defaultErrorFunc) {
  return function requiredParam(str) {
    return errorFunc(str)
  }
}

module.exports = required

'use strict'

const remitOne = require('./index')

const makeRequest = remitOne({
    baseUrl: 'https://***.remitone.com/ws',
    pin: '****',
    username: '*****',
    password: "******"
})

makeRequest({
    group: 'transaction',
    method: 'getPayoutTransactions',
    params: {  }
}).then(res => console.log(JSON.stringify(res))).catch(console.log)

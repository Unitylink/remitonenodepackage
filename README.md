# RemitOne SDK

This package is a wrapper for interacting with the remit one API.
You'll need to go though the [remiteOne documentation](https://www.unitylinkonline.com/images/RemitONE_Remitter_WebServices_Specification.pdf) for the various groups and their methods.

## Installation

```sh
> npm i remitone-api
```

## Usage

```javascript
const remitOneSDK = require('remitone-api')

const makeRequest = remitOneSDK({ 
    baseUrl: 'https://******.remitone.com/ws',
    pin: '******',
    username: '******',
    password: '******'
})

const makeRequest({ 
    group: 'rates',
    method: 'getRates',
    params: { dest_country: 'ghana' }
}).then(res => console.log(JSON.stringify(res))).catch(console.log)
```

## Response

The library returns results in a json format

Eg.
```
{
  "responseId": "25579159",
  "status": "SUCCESS",
  "result": {
    "rate": {
      "account": "6.5",
      "cash_collection": "7.9621",
      "card": "6.5",
      "home_delivery": "5.95111111",
      "utility_bill": "6.5",
      "mobile_transfer": "6.5"
    },
    "source_country": "United Kingdom",
    "source_currency": {},
    "dest_country": "Ghana",
    "dest_currency": {}
  }
}
```


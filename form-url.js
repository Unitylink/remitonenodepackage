'use strict'

const formUrl = (baseUrl, group, method) => {
    const uri = `${baseUrl}/${group}/${method}`
    return uri
  }
  
module.exports = formUrl

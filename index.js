'use strict'

const required = require('./lib/required')()
const formUrl = require('./form-url')
const request = require('./request')

const unityLinkRemitOne = ({
  baseUrl = required('baseUrl'),
  pin = required('pin'),
  username = required('username'),
  password = required('password')
}) => ({
  group = required('group'),
  method = required('method'),
  params = {}
}) => {
  const uri = formUrl(baseUrl, group, method)
  const body = Object.assign(params, { pin, username, password })
  return request({ uri, body })
}

module.exports = unityLinkRemitOne

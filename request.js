'use strict'

const xml2js = require('xml2js')
const makeCallToRemitOne = require('./lib/make-call-to-remit-one')
const asyncPipe = require('./lib/async-pipe')

const parser = new xml2js.Parser({ ignoreAttrs: true, explicitArray: false })

const getBody = ({ body }) => body

const request = (options) => asyncPipe(makeCallToRemitOne, getBody, parser.parseStringPromise)(options)

module.exports = request
